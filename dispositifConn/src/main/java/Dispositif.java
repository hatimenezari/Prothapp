
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;


public class Dispositif {

    static int idProthese = 3;
    static String codePatient = "sdfasdfaf";
    static double temperature;
    static double pression;

    public static void main(String[] args) throws Exception {
        while(true){

            temperature = (Math.random()*2)+36;
            pression = (Math.random()*20)+40;

            String payload = "{" +
                    "\"idProthese\": \""+idProthese+"\", " +
                    "\"codePatient\": \""+codePatient+"\", " +
                    "\"temperature\": \""+temperature+"\", " +
                    "\"pression\": \""+pression+"\" " +
                    "}";

            StringEntity entity = new StringEntity(payload,
                    ContentType.APPLICATION_FORM_URLENCODED);

            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost("http://localhost:8080/dispProxy");
            request.addHeader("content-type", "application/json;charset=UTF-8");
            request.setEntity(entity);

            HttpResponse response = httpClient.execute(request);
            System.out.println(response.getStatusLine());
            Thread.sleep(10000);
        }
    }
}