package prometheus.demo.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

import javax.persistence.*;
import java.util.List;

@Entity

public class Donnee {
    @Id
    @GeneratedValue
    int id;
    double temperature;
    double pression;
    @ManyToOne
    Patient patient;
    @ManyToOne
    Prothese prothese;

    public Donnee() {
    }

    public Donnee(double temperature, double pression, Patient patient, Prothese prothese) {
        this.temperature = temperature;
        this.pression = pression;
        this.patient = patient;
        this.prothese = prothese;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getPression() {
        return pression;
    }

    public void setPression(double pression) {
        this.pression = pression;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Prothese getProthese() {
        return prothese;
    }

    public void setProthese(Prothese prothese) {
        this.prothese = prothese;
    }
}
