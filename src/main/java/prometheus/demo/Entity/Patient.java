package prometheus.demo.Entity;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Patient {
    @Id
    @GeneratedValue
    private int id;
    private String nom;
    private String codePatient;
    public double poid;
    public double taille;
    public double longueurMain;
    public double longueurPied;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name= "patient_id")
    @JsonIgnore
    List<Commande> commandes;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name= "patient_id")
    @JsonIgnore
    List<Donnee> donneeProthesePatients;

    public Patient() {
    }

    public Patient(String nom, String codePatient, double poid, double taille, double longueurMain, double longueurPied, List<Commande> commandes, List<Donnee> donneeProthesePatients) {
        this.nom = nom;
        this.codePatient = codePatient;
        this.poid = poid;
        this.taille = taille;
        this.longueurMain = longueurMain;
        this.longueurPied = longueurPied;
        this.commandes = commandes;
        this.donneeProthesePatients = donneeProthesePatients;
    }

    public List<Donnee> getDonneeProthesePatients() {
        return donneeProthesePatients;
    }

    public void setDonneeProthesePatients(List<Donnee> donneeProthesePatients) {
        this.donneeProthesePatients = donneeProthesePatients;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCodePatient() {
        return codePatient;
    }

    public void setCodePatient(String codePatient) {
        this.codePatient = codePatient;
    }

    public double getPoid() {
        return poid;
    }

    public void setPoid(double poid) {
        this.poid = poid;
    }

    public double getTaille() {
        return taille;
    }

    public void setTaille(double taille) {
        this.taille = taille;
    }

    public double getLongueurMain() {
        return longueurMain;
    }

    public void setLongueurMain(double longueurMain) {
        this.longueurMain = longueurMain;
    }

    public double getLongueurPied() {
        return longueurPied;
    }

    public void setLongueurPied(double longueurPied) {
        this.longueurPied = longueurPied;
    }

    public List<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }
}
