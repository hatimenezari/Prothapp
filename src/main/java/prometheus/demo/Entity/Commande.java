package prometheus.demo.Entity;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Commande {

    @Id
    @GeneratedValue
    int id;
    @ManyToOne
    Patient patient;
    @ManyToOne
    Prothese prothese;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    LocalDateTime dateCommande;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    LocalDateTime datePrevu;

    public Commande() {
    }

    public Commande(int id, Patient patient, Prothese prothese, LocalDateTime dateCommande, LocalDateTime datePrevu) {
        this.id = id;
        this.patient = patient;
        this.prothese = prothese;
        this.dateCommande = dateCommande;
        this.datePrevu = datePrevu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Prothese getProthese() {
        return prothese;
    }

    public void setProthese(Prothese prothese) {
        this.prothese = prothese;
    }

    public LocalDateTime getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(LocalDateTime dateCommande) {
        this.dateCommande = dateCommande;
    }

    public LocalDateTime getDatePrevu() {
        return datePrevu;
    }

    public void setDatePrevu(LocalDateTime datePrevu) {
        this.datePrevu = datePrevu;
    }
}
