package prometheus.demo.Entity;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Prothese {
    @Id
    @GeneratedValue
    private int id;
    private String model;
    @Lob
    @Column(columnDefinition="LONGTEXT")
    private String base64Image;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "prothese_id")
    List<Commande> commandes;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name= "prothese_id")
    List<Donnee> donneeProthesePatients;

    public Prothese() {
    }


    public Prothese(String model, String base64Image, List<Commande> commandes, List<Donnee> donneeProthesePatients) {
        this.model = model;
        this.base64Image = base64Image;
        this.commandes = commandes;
        this.donneeProthesePatients = donneeProthesePatients;
    }

    public List<Donnee> getDonneeProthesePatients() {
        return donneeProthesePatients;
    }

    public void setDonneeProthesePatients(List<Donnee> donneeProthesePatients) {
        this.donneeProthesePatients = donneeProthesePatients;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public List<Commande> getCommandes() {
        return commandes;
    }


    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }
}


