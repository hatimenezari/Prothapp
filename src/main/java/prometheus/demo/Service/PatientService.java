package prometheus.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import prometheus.demo.Entity.Patient;
import prometheus.demo.Entity.Prothese;
import prometheus.demo.Repository.PatientRepository;
import prometheus.demo.Repository.ProtheseRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class PatientService {

    @Autowired
    public PatientRepository rep;


    public List<Patient> getPatients() {
        List<Patient> patients = new ArrayList<>();
        rep.findAll().forEach(k -> patients.add(k));
        return patients;
    }

    public Patient getPatient(int id) {
        return rep.findById(id).get();
    }

    public void addPatient(Patient patient) {
        rep.save(patient);
    }

    public void updatePatient(Patient patient) {
        rep.save(patient);
    }

    public void deletePatient(Patient patient) {
        rep.delete(patient);
    }

    public Patient getPatientByCode(String code) {
        return rep.findByCodePatient(code);
    }
}
