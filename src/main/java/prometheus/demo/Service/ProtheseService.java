package prometheus.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import prometheus.demo.Entity.Prothese;
import prometheus.demo.Repository.ProtheseRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProtheseService {

    @Autowired
    public ProtheseRepository rep;

    public List<Prothese> getProtheses() {
        List<Prothese> protheses = new ArrayList<>();
        rep.findAll().forEach(k -> protheses.add(k));
        return protheses;
    }

    public Prothese getProthese(int id) {

        return rep.findById(id).get();
    }

    public void addProthese(Prothese prothese) {
        rep.save(prothese);
    }

    public void updateProthese(Prothese prothese) {
        rep.save(prothese);
    }

    public void deleteProthese(Prothese prothese) {
        rep.delete(prothese);
    }

    public List<String> getProthesesModels() {
        List<String> protheses = new ArrayList<>();
        rep.findAllModels().forEach(k -> protheses.add(k));
        return protheses;
    }

    public List<Prothese> findProtheseByModel(String model) {
        List<Prothese> protheses = new ArrayList<>();
        rep.findAllByModel(model).forEach(k -> protheses.add(k));
        return protheses;
    }
}
