package prometheus.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import prometheus.demo.Entity.Commande;
import prometheus.demo.Entity.Donnee;
import prometheus.demo.Entity.Patient;
import prometheus.demo.Entity.Prothese;
import prometheus.demo.Repository.CommandeRepository;
import prometheus.demo.Repository.DonneeProthesePatientRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class DonneeProthesePatientService implements Observer
{
    @Autowired
    Facade facade;
    @Autowired
    public DonneeProthesePatientRepository rep;

    public List<Donnee> getDonneProthesePatients(int idPatient, int idProthese) {
        List<Donnee> donnees = new ArrayList<>();
        rep.findAllByPatientIdAndProtheseId(idPatient, idProthese).forEach(k -> {
            donnees.add(k);
        });
        return donnees;
    }


    @Override
    public void update(double temperature, double pression, String codePatient, int idProthese) {
        Patient patient = facade.getPatientByCode(codePatient);
        Prothese prothese = facade.getProthese(idProthese);
        Donnee donnee = new Donnee(temperature, pression, patient, prothese);
        rep.save(donnee);
    }
}
