package prometheus.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import prometheus.demo.Entity.Patient;
import prometheus.demo.Entity.Prothese;

import java.util.ArrayList;
import java.util.List;

public class RemoteProxy implements DispositifConnexion {


    List<Observer> observers = new ArrayList<>();
    int idProthese;
    String codePatient;
    double temperature;
    double pression;



    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(k -> k.update(temperature, pression, codePatient, idProthese));
    }



    public List<Observer> getObservers() {
        return observers;
    }

    public void setObservers(List<Observer> observers) {
        this.observers = observers;
    }

    public int getIdProthese() {
        return idProthese;
    }

    public void setIdProthese(int idProthese) {
        this.idProthese = idProthese;
    }

    public String getCodePatient() {
        return codePatient;
    }

    public void setCodePatient(String codePatient) {
        this.codePatient = codePatient;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getPression() {
        return pression;
    }

    public void setPression(double pression) {
        this.pression = pression;
    }
}
