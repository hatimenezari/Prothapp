package prometheus.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import prometheus.demo.Entity.Commande;
import prometheus.demo.Entity.Donnee;
import prometheus.demo.Entity.Patient;
import prometheus.demo.Entity.Prothese;

import java.util.List;

@Service
public class Facade {

    @Autowired
    ProtheseService Protheseservice;
    @Autowired
    CommandeService Commandeservice;
    @Autowired
    PatientService Patientservice;
    @Autowired
    DonneeProthesePatientService DonneesService;

    public List<Prothese> getProtheses(){
        return Protheseservice.getProtheses();
    }

    public Prothese getProthese(int id){
        return Protheseservice.getProthese(id);
    }

    public void addProthese(Prothese prothese){
        Protheseservice.addProthese(prothese);
    }

    public void updateProthese(Prothese prothese){
        Protheseservice.updateProthese(prothese);
    }

    public void deleteProthese(Prothese prothese){
        Protheseservice.deleteProthese(prothese);
    }

    public List<String> getProthesesModels() {
        return Protheseservice.getProthesesModels();
    }

    public List<Patient> getPatients() {
        return Patientservice.getPatients();
    }

    public Patient getPatient(int id) {
        return Patientservice.getPatient(id);
    }

    public Patient getPatientByCode(String code) {
        return Patientservice.getPatientByCode(code);
    }

    public void addPatient(Patient patient) {
        Patientservice.addPatient(patient);
    }


    public void updatePatient(Patient patient) {
        Patientservice.addPatient(patient);
    }


    public void deletePatient(Patient patient) {
        Patientservice.deletePatient(patient);
    }


    public List<Commande> getCommandes() {
        return Commandeservice.getCommandes();
    }

    public Commande getCommande(int id) {
        return Commandeservice.getCommande(id);
    }

    public void addCommande(Commande commande) {
        Commandeservice.addCommande(commande);
    }

    public void updateCommande(Commande commande) {
        Commandeservice.updateCommande(commande);
    }

    public void deleteCommande(Commande commande) {
        Commandeservice.deleteCommande(commande);
    }


    public List<Prothese> getProthesesByModels(String model) {
        return Protheseservice.findProtheseByModel(model);
    }


    public List<Donnee> getDonneeProthesePatient(int idPatient, int idProthese) {
        return DonneesService.getDonneProthesePatients(idPatient, idProthese);
    }


    public void addDonneeRecu(RemoteProxy remoteProxy) {
        remoteProxy.addObserver(DonneesService);
        remoteProxy.notifyObservers();
    }
}
