package prometheus.demo.Service;

public interface Observer {
    public void update(double temperature, double pression, String codePatient, int idProthese);
}
