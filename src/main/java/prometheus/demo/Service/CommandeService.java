package prometheus.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import prometheus.demo.Entity.Commande;
import prometheus.demo.Entity.Prothese;
import prometheus.demo.Repository.CommandeRepository;
import prometheus.demo.Repository.ProtheseRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommandeService {

    @Autowired
    public CommandeRepository rep;

    public List<Commande> getCommandes() {
        List<Commande> commandes = new ArrayList<>();
        rep.findAll().forEach(k -> commandes.add(k));
        return commandes;
    }

    public Commande getCommande(int id) {
        return rep.findById(id).get();
    }

    public void addCommande(Commande commande) {
        rep.save(commande);
    }

    public void updateCommande(Commande commande) {
        rep.save(commande);
    }

    public void deleteCommande(Commande commande) {
        rep.delete(commande);
    }
}
