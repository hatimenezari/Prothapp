package prometheus.demo.Repository;

import org.springframework.data.repository.CrudRepository;
import prometheus.demo.Entity.Commande;

public interface CommandeRepository extends CrudRepository<Commande,Integer> {

}
