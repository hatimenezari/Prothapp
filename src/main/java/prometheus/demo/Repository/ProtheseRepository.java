package prometheus.demo.Repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import prometheus.demo.Entity.Prothese;

import java.util.List;

public interface ProtheseRepository extends CrudRepository<Prothese, Integer> {
    @Query("SELECT Distinct p.model FROM Prothese p")
    List<String> findAllModels();


    List<Prothese> findAllByModel(String model);
}
