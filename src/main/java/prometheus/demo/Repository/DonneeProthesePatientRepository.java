package prometheus.demo.Repository;

import org.springframework.data.repository.CrudRepository;
import prometheus.demo.Entity.Donnee;

import java.util.List;

public interface DonneeProthesePatientRepository extends CrudRepository<Donnee,Integer> {

    List<Donnee> findAllByPatientIdAndProtheseId(int idPatient, int idProthese);
}
