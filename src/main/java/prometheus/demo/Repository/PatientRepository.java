package prometheus.demo.Repository;

import org.springframework.data.repository.CrudRepository;
import prometheus.demo.Entity.Patient;

public interface PatientRepository extends CrudRepository<Patient, Integer> {
    public Patient findByCodePatient(String codePatient);
}
