package prometheus.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProthappApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProthappApplication.class, args);
    }
}
