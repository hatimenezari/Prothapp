package prometheus.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import prometheus.demo.Entity.Commande;
import prometheus.demo.Entity.Patient;
import prometheus.demo.Service.Facade;

import java.util.List;

@RestController
@CrossOrigin("*")
public class CommandesController {

    @Autowired
    Facade facade;

    @RequestMapping("/commandes")
    public List<Commande> getCommandes(){
        return facade.getCommandes();
    }

    @RequestMapping("/commandes/{id}")
    public Commande getCommande(@PathVariable int id){
        return facade.getCommande(id);
    }

    @RequestMapping(method = RequestMethod.POST, value="/commandes")
    public void addCommande(@RequestBody Commande commande){
        facade.addCommande(commande);
    }

    @RequestMapping(method = RequestMethod.PUT, value="/commandes")
    public void updateCommande(@RequestBody Commande commande){
        facade.updateCommande(commande);
    }

    @RequestMapping(method = RequestMethod.DELETE, value="/commandes")
    public void deleteCommande(@RequestBody Commande commande){
        facade.deleteCommande(commande);
    }
}
