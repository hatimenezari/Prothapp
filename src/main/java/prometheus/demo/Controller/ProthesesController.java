package prometheus.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import prometheus.demo.Entity.Prothese;
import prometheus.demo.Service.Facade;
import sun.misc.BASE64Decoder;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin("*")
public class ProthesesController {

    @Autowired
    Facade facade;

    @RequestMapping("/protheses")
    public List<Prothese> getProtheses(){
        return facade.getProtheses();
    }

    @RequestMapping("/prothesesModels")
    public List<String> getProthesesModels(){
        return facade.getProthesesModels();
    }

    @RequestMapping("/prothesesByModels/{model}")
    public List<Prothese> getProthesesByModels(@PathVariable String model){
        return facade.getProthesesByModels(model);
    }

    @RequestMapping("/protheses/{id}")
    public Prothese getProthese(@PathVariable int id){
        return facade.getProthese(id);
    }

    @RequestMapping(method = RequestMethod.POST, value="/protheses")
    public void addProthese(@RequestBody Prothese prothese){
        facade.addProthese(prothese);
    }

    @RequestMapping(method = RequestMethod.PUT, value="/protheses")
    public void updateProthese(@RequestBody Prothese prothese){
        facade.updateProthese(prothese);
    }

    @RequestMapping(method = RequestMethod.DELETE, value="/protheses")
    public void deleteProthese(@RequestBody Prothese prothese){
        facade.deleteProthese(prothese);
    }

}
