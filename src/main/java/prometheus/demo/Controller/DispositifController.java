package prometheus.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import prometheus.demo.Entity.Donnee;
import prometheus.demo.Service.Facade;
import prometheus.demo.Service.RemoteProxy;

import java.util.List;

@RestController
@CrossOrigin("*")
public class DispositifController {

    @Autowired
    Facade facade;

    @RequestMapping(method = RequestMethod.POST, value="/dispProxy")
    public void addDispositif(@RequestBody RemoteProxy remoteProxy){
        facade.addDonneeRecu(remoteProxy);
    }


}
