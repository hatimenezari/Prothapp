package prometheus.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import prometheus.demo.Entity.Patient;
import prometheus.demo.Entity.Prothese;
import prometheus.demo.Service.Facade;

import java.util.List;

@RestController
@CrossOrigin("*")
public class PatientsController {
    @Autowired
    Facade facade;

    @RequestMapping("/patients")
    public List<Patient> getPatients(){
        return facade.getPatients();
    }

    @RequestMapping("/patients/{id}")
    public Patient getPatient(@PathVariable int id){
        return facade.getPatient(id);
    }

    @RequestMapping(method = RequestMethod.POST, value="/patients")
    public void addPatient(@RequestBody Patient patient){
        facade.addPatient(patient);
    }

    @RequestMapping(method = RequestMethod.PUT, value="/patient")
    public void updatePatient(@RequestBody Patient patient){
        facade.updatePatient(patient);
    }

    @RequestMapping(method = RequestMethod.DELETE, value="/patient")
    public void deletePatient(@RequestBody Patient patient){
        facade.deletePatient(patient);
    }
}
