package prometheus.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import prometheus.demo.Entity.Commande;
import prometheus.demo.Entity.Donnee;
import prometheus.demo.Service.Facade;

import java.util.List;

@RestController
@CrossOrigin("*")
public class DonneeProthesePatientController {

    @Autowired
    Facade facade;

    @RequestMapping("/donnees/{idPatient}/{idProthese}")
    public List<Donnee> getCommandes(@PathVariable int idPatient, @PathVariable int idProthese){
        return facade.getDonneeProthesePatient(idPatient, idProthese);
    }


}
