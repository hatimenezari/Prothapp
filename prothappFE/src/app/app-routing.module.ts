///<reference path="../../node_modules/@angular/core/src/metadata/ng_module.d.ts"/>
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {PatientComponent} from './patient/patient.component';
import {CreatePatientComponent} from './create-patient/create-patient.component';
import {CommandeComponent} from './commande/commande.component';
import {ProtheseComponent} from './prothese/prothese.component';
import {ListcommandeComponent} from './listcommande/listcommande.component';
import {DonneeComponent} from './donnee/donnee.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'patients', component: PatientComponent },
  { path: 'createpatient', component: CreatePatientComponent },
  { path: 'protheses', component: ProtheseComponent },
  { path: 'commande', component: CommandeComponent },
  { path: 'commandes', component: ListcommandeComponent },
  { path: 'donnees', component: DonneeComponent },
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forRoot(routes)
  ],
  declarations: [],

})



export class AppRoutingModule {

}
