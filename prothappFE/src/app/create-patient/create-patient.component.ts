import { Component, OnInit } from '@angular/core';
import {Patient} from '../patient/Patient';
import {PatientService} from '../services/patient.service';

@Component({
  selector: 'app-create-patient',
  templateUrl: './create-patient.component.html',
  styleUrls: ['./create-patient.component.css']
})
export class CreatePatientComponent implements OnInit {
  patient: Patient;
  showAddedMessage: boolean = false;

  constructor(public service:PatientService) {
    this.patient = new Patient(0,"","",0,0,0,0);
  }

  onClickAddPatient(){
    this.service.addPatient(this.patient);
    window.location.reload();
    this.showAddedMessage = true;

  }

  ngOnInit() {
  }

}
