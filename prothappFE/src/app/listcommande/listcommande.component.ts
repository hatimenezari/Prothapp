import { Component, OnInit } from '@angular/core';
import {Patient} from '../patient/Patient';
import {PatientService} from '../services/patient.service';
import {CommandeService} from '../services/commande.service';
import {Commande} from '../commande/commande';

@Component({
  selector: 'app-listcommande',
  templateUrl: './listcommande.component.html',
  styleUrls: ['./listcommande.component.css']
})
export class ListcommandeComponent implements OnInit {
  commandes: Commande[]=[] ;
  patient:Patient;
  constructor(public service:CommandeService, public pService: PatientService) {

  }

  getCommandes(){
    this.service.getCommandes().subscribe((data: Commande[]) => {
      this.commandes = data;
    });

  }



  ngOnInit() {
    this.getCommandes();
  }

}
