import { Component, OnInit } from '@angular/core';
import {PatientService} from '../services/patient.service';
import {Patient} from '../patient/Patient';
import {Prothese} from './prothese';
import {ProtheseService} from '../services/prothese.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-prothese',
  templateUrl: './prothese.component.html',
  styleUrls: ['./prothese.component.css']
})
export class ProtheseComponent implements OnInit {
  protheses:Prothese[] ;


  constructor(public service:ProtheseService) {
  }

  getProtheses(){
    this.service.getProtheses()
      .subscribe((data: Prothese[]) => this.protheses = data);
    console.log(this.protheses[0].id);
  }

  ngOnInit() {
    this.getProtheses();
  }

}
