import { Component, OnInit } from '@angular/core';
import {PatientService} from '../services/patient.service';
import {ProtheseService} from '../services/prothese.service';
import {Prothese} from '../prothese/prothese';
import {Patient} from '../patient/Patient';
import {Commande} from './commande';
import {CommandeService} from '../services/commande.service';
import {DatePipe} from '@angular/common';


@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.css']
})
export class CommandeComponent implements OnInit {

  hidden = true;
  prothesesByModel: Prothese[];
  prothesesModels: string[];
  date: Date = new Date();
  date2: Date = new Date(this.date.getTime() + (1000*60*60*24));

  show(m:string){

    this.hidden = false;
    this.protheseService.getProtheseByModel(m).subscribe((data: Prothese[]) => this.prothesesByModel = data);

  }

  createcommande(patient: Patient, prothese: Prothese){
    this.date= new Date();
    this.date2 = new Date(this.date.getTime() + (7*1000*60*60*24));
    let command: Commande = new Commande(0,patient, prothese,this.datePipe.transform( this.date, "yyyy-MM-dd HH:mm"), this.datePipe.transform( this.date2, "yyyy-MM-dd HH:mm"));
    this.commandeService.addCommande(command);
    alert("commande créée avec succès pour le patient " + patient.nom + " pour le model "+ prothese.model);
  }

  getModels(){
    this.protheseService.getProthesesModels().subscribe((data: string[]) => this.prothesesModels = data);
  }

  constructor(public patientService: PatientService, public protheseService:ProtheseService,public commandeService:CommandeService,private datePipe: DatePipe) { }

  ngOnInit() {
    this.getModels();
  }

}
