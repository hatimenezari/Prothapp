import {Patient} from '../patient/Patient';
import {Prothese} from '../prothese/prothese';

export class Commande{

  constructor(public id:number, public patient:Patient, public prothese:Prothese, public dateCommande: string, public  datePrevu:string){

  }

}
