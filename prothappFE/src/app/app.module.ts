import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import {BsDropdownModule} from 'ngx-bootstrap';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { PatientComponent } from './patient/patient.component';
import {RouterModule} from '@angular/router';
import { CreatePatientComponent } from './create-patient/create-patient.component';
import { CommandeComponent } from './commande/commande.component';
import { ProtheseComponent } from './prothese/prothese.component';
import { ListcommandeComponent } from './listcommande/listcommande.component';
import {HttpClientModule} from '@angular/common/http';
import { DonneeComponent } from './donnee/donnee.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PatientComponent,
    CreatePatientComponent,
    CommandeComponent,
    ProtheseComponent,
    ListcommandeComponent,
    DonneeComponent
  ],
  imports: [
    BrowserModule, FormsModule, NgbModule.forRoot(),BsDropdownModule.forRoot(), AppRoutingModule, RouterModule,HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
