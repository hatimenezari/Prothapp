import {Patient} from '../patient/Patient';
import {Prothese} from '../prothese/prothese';

export class Donnee{

  constructor(public id:number, public temperature: number, public pression: number, public patient:Patient, public prothese:Prothese){

  }

}
