import { Component, OnInit } from '@angular/core';
import {PatientService} from '../services/patient.service';
import {DonneeService} from '../services/donnee.service';
import {Patient} from '../patient/Patient';
import {Donnee} from './Donnee';

@Component({
  selector: 'app-donnee',
  templateUrl: './donnee.component.html',
  styleUrls: ['./donnee.component.css']
})
export class DonneeComponent implements OnInit {

  hidden:boolean = true;
  patients:Patient[] = [];
  donnees:Donnee[]= [];

  constructor(public pService:PatientService, public service:DonneeService) { }

  getPatients(){
    this.pService.getPatients().subscribe((data: Patient[]) => this.patients = data);
  }

  getDonnee(p: Patient){
    this.service.getDonnees(p, 3).subscribe((data: Donnee[]) => this.donnees = data);
  }


  show(p: Patient){
    this.hidden = false;
    this.pService.selectedPatient = p;
    this.getDonnee(p);
  }

  ngOnInit() {
    this.getPatients();
  }

}
