import { Injectable } from '@angular/core';
import {Patient} from '../patient/Patient';
import {HttpClient} from '@angular/common/http';
import {Prothese} from '../prothese/prothese';

@Injectable({
  providedIn: 'root'
})
export class DonneeService {

  constructor(public http: HttpClient) { }

  getDonnees(patient:Patient, prothese:number){
    return this.http.get("http://localhost:8080/donnees/"+patient.id+"/"+prothese);
  }


}
