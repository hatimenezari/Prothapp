import {Injectable, OnInit} from '@angular/core';
import {Patient} from '../patient/Patient';
import {Http} from '@angular/http';
import {map} from 'rxjs/operators';
import {Prothese} from '../prothese/prothese';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProtheseService {

  constructor(public http: HttpClient) {

  }


  getProtheses(){
    return this.http.get("http://localhost:8080/protheses");
  }

  getProtheseByModel(model:string){
    return this.http.get("http://localhost:8080/prothesesByModels/"+model);
  }

  getProthesesModels(){
    return  this.http.get("http://localhost:8080/prothesesModels");
  }

}
