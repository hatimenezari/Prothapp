import { TestBed, inject } from '@angular/core/testing';

import { ProtheseService } from './prothese.service';

describe('ProtheseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProtheseService]
    });
  });

  it('should be created', inject([ProtheseService], (service: ProtheseService) => {
    expect(service).toBeTruthy();
  }));
});
