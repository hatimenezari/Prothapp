import {Injectable, OnInit} from '@angular/core';
import {Patient} from '../patient/Patient';
import {Http} from '@angular/http';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  selectedPatient: Patient;

  constructor(public http: HttpClient) {
  }

  getPatients(){
    return this.http.get("http://localhost:8080/patients");
  }

  addPatient(p:Patient){
    p.codePatient ="generatedCode";
    return this.http.post("http://localhost:8080/patients", (p)).subscribe();
  }

  getPatient(id:number){
    return this.http.get("http://localhost:8080/patients/" + id);
  }

}
