import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {map} from 'rxjs/operators';
import {Commande} from '../commande/commande';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommandeService {

  constructor(public http: HttpClient) {

  }

  getCommandes(){
    return this.http.get("http://localhost:8080/commandes");
  }

  addCommande(commande: Commande){
    this.http.post("http://localhost:8080/commandes", (commande)).subscribe();
  }
}
