import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  images: Array<string>;

  ngOnInit(): void {
    this.images = ["assets/img/proth1.jpg","assets/img/proth0.jpg","assets/img/proth2.jpg"]
  }

  constructor() { }

}
