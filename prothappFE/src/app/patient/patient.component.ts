import { Component, OnInit } from '@angular/core';
import {Patient} from './Patient';
import {PatientService} from '../services/patient.service';
import {Prothese} from '../prothese/prothese';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  patients:Patient[] ;

  constructor(public service:PatientService) {

  }

  getPatients(){
    this.service.getPatients().subscribe((data: Patient[]) => this.patients = data);
  }

  selection(p:Patient){
    this.service.selectedPatient = p;
  }

  ngOnInit() {
    this.getPatients();
  }


}
